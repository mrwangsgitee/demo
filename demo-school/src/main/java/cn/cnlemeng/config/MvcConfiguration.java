package cn.cnlemeng.config;

import cn.cnlemeng.interceptor.LoginInterceptor;
import cn.cnlemeng.interceptor.PermissionInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class MvcConfiguration implements WebMvcConfigurer {

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        List<String> permissionList = new ArrayList<>();
        permissionList.add("/query/subjectScore/{year}");
        permissionList.add("/query/allTeacherScore/{currentPage}/{maxCount}/{year}");
        registry.addInterceptor(new PermissionInterceptor()).addPathPatterns(permissionList);
        registry.addInterceptor(new LoginInterceptor()).addPathPatterns("/*/**")
                .excludePathPatterns("/login","/doc.html","/swagger-resources/configuration/ui","/swagger-resources");
    }
}
