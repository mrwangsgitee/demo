package cn.cnlemeng.config;


import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static springfox.documentation.builders.PathSelectors.any;

@EnableSwagger2
@Configuration
public class SwaggerConfig
{



    private ApiInfo apiInfo() {
    	return new ApiInfoBuilder().title("测试demo").version("1.0").build();
    }

	private List<ApiKey> securitySchemes() {
		List<ApiKey> apiKeyList= new ArrayList<>();
		apiKeyList.add(new ApiKey("book-code", "book-code", "header"));
		apiKeyList.add(new ApiKey("Authorization", "Authorization", "header"));
		apiKeyList.add(new ApiKey("accountNum", "accountNum", "header"));
		return apiKeyList;
	}

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.directModelSubstitute(LocalDateTime.class, String.class)
				.directModelSubstitute(LocalDate.class, String.class)
				.directModelSubstitute(LocalTime.class, String.class)
				.useDefaultResponseMessages(false)
				.select()

				.apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
				.paths(any())
				.build()
//				.securityContexts(securityContexts())
				.apiInfo(apiInfo())
				.securitySchemes(securitySchemes());
	}



}
