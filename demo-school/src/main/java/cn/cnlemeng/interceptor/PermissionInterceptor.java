package cn.cnlemeng.interceptor;

import cn.cnlemeng.constant.SessionConstant;
import cn.cnlemeng.constant.TeacherConstant;
import cn.cnlemeng.exception.PermissionException;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class PermissionInterceptor implements HandlerInterceptor {
    /**
     * 普通教师，教导主任权限控制
     * @param request
     * @param response
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HttpSession session = request.getSession();
        Object attribute = session.getAttribute(SessionConstant.TEACHER_RANK_SESSION);
        if (null == attribute || !attribute.equals(TeacherConstant.TeacherRankEnum.DIRECTOR_TEACHER.getCode())){
            throw new PermissionException("权限不足");
        }
        return true;
    }
}
