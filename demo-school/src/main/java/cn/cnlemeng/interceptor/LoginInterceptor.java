package cn.cnlemeng.interceptor;

import cn.cnlemeng.constant.SessionConstant;
import cn.cnlemeng.exception.NotLoginException;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class LoginInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        HttpSession session = request.getSession();
        Object attribute = session.getAttribute(SessionConstant.TEACHER_ID_SESSION);
        if (null == attribute){
            throw new NotLoginException("用户未登录");
        }
        return true;
    }
}
