package cn.cnlemeng.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
public class TeacherSubjectRelation implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Long id;

    private Long teacherId;

    private Long subjectId;

    private Date createAt;
}