package cn.cnlemeng.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
public class Teacher implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Long teacherId;

    private String teacherName;

    private Integer teacherRank;

    private Date createdAt;
}