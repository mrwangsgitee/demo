package cn.cnlemeng.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@ToString
@Getter
@Setter
@Entity
public class StudentScore implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Long scoreId;

    private Integer year;

    private Long studentId;

    private Long subjectId;

    private Double score;

    private Date createAt;
}