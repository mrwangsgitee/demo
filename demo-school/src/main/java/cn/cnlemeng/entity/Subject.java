package cn.cnlemeng.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@ToString
@Getter
@Setter
@Entity
public class Subject implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Long subjectId;

    private String subjectName;

    private Date createdAt;

}