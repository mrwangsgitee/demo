package cn.cnlemeng.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Entity
@ToString
public class Student implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    private Long studentId;

    private String studentName;

    private Date createAt;
}