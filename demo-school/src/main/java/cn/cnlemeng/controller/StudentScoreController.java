package cn.cnlemeng.controller;

import cn.cnlemeng.constant.SessionConstant;
import cn.cnlemeng.dto.StudentYearScoreDto;
import cn.cnlemeng.dto.SubjectScoreDto;
import cn.cnlemeng.dto.TeacherYearScoreDto;
import cn.cnlemeng.entity.StudentScore;
import cn.cnlemeng.service.StudentScoreService;
import cn.cnlemeng.util.PageInfo;
import cn.cnlemeng.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@Api(tags = "学生分数管理")
public class StudentScoreController {
    @Autowired
    private StudentScoreService studentScoreService;

    /**
     * 保存学生分数
     *
     * @param studentScore
     * @return
     */
    @ApiOperation("保存学生分数")
    @PostMapping("/save/studentScore")
    public R saveStudentScore(@RequestBody StudentScore studentScore) {
        studentScoreService.save(studentScore);
        return R.ok();
    }

    /**
     * 修改学生分数
     *
     * @param studentScore
     * @return
     */
    @ApiOperation("修改学生分数")
    @PostMapping("/update/studentScore")
    public R updateStudentScore(@RequestBody StudentScore studentScore) {
        studentScoreService.updateById(studentScore);
        return R.ok();
    }

    /**
     * 删除学生分数
     *
     * @param studentScoreId
     * @return
     */
    @ApiOperation("删除学生分数")
    @PostMapping("/delete/studentScore")
    public R deleteStudentScoreById(@RequestBody Long studentScoreId) {
        studentScoreService.deleteStudentScoreById(studentScoreId);
        return R.ok();
    }

    /**
     * 根据老师的id以及学年查询教师本人每学年，学科平均成绩，最高分，最低分
     *
     * @param year
     * @return
     */
    @ApiOperation("查询老师学科分数")
    @GetMapping("/query/teacherScore/{year}")
    public R readTeacherYearScoreListByIdAndYear(@PathVariable("year") Integer year, HttpSession session) {
        Long teacherId = (Long) session.getAttribute(SessionConstant.TEACHER_ID_SESSION);
        TeacherYearScoreDto teacherYearScoreDto = studentScoreService.readTeacherYearScoreListByIdAndYear(teacherId, year);
        return R.ok().put("data", teacherYearScoreDto);
    }

    /**
     * 教导主任查询所有老师年度成绩,分页查询
     *
     * @param year
     * @return
     */
    @ApiOperation("分页查询所有老师学科分数")
    @GetMapping("/query/allTeacherScore/{currentPage}/{maxCount}/{year}")
    public R batchReadAllTeacherYearScoreListByYear(@PathVariable("currentPage") Integer currentPage, @PathVariable Integer maxCount, @PathVariable("year") Integer year) {
        PageInfo<TeacherYearScoreDto> pageInfo = studentScoreService.batchReadAllTeacherYearScoreListByYear(currentPage, maxCount, year);
        return R.ok().put("data", pageInfo);
    }

    /**
     * 根据学生id以及学年查询本人每学年各学科成绩
     *
     * @param studentId
     * @param year
     * @return
     */
    @ApiOperation("查询学生分数")
    @GetMapping("/query/studentScore/{id}/{year}")
    public R readStudentYearScoreByIdAndYear(@PathVariable("id") Long studentId, @PathVariable("year") Integer year) {
        StudentYearScoreDto studentYearScoreDto = studentScoreService.readStudentYearScoreByIdAndYear(studentId, year);
        return R.ok().put("data", studentYearScoreDto);
    }

    /**
     * 教导主任批量查询每学年学科的成绩
     *
     * @return
     */
    @ApiOperation("查询每年所有学科分数")
    @GetMapping("/query/subjectScore/{year}")
    public R batchReadSubjectScoreList(@PathVariable("year") Integer year) {
        List<SubjectScoreDto> subjectScoreDtos = studentScoreService.batchReadSubjectScoreList(year);
        return R.ok().put("data", subjectScoreDtos);
    }

}
