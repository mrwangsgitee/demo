package cn.cnlemeng.controller;

import cn.cnlemeng.entity.Student;
import cn.cnlemeng.exception.BizCodeEnum;
import cn.cnlemeng.exception.DeleteException;
import cn.cnlemeng.service.StudentService;
import cn.cnlemeng.util.PageInfo;
import cn.cnlemeng.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@Api(tags = "学生管理")
public class StudentController {
    @Autowired
    private StudentService studentService;

    /**
     * 保存学生
     * @param student
     * @return
     */
    @ApiOperation("添加学生" )
    @PostMapping("/save/student")
    public R saveStudent(@RequestBody Student student) {
        student.setCreateAt(new Date());
        studentService.saveStudent(student);
        return R.ok();
    }

    /**
     * 更新学生名字
     * @param student
     * @return
     */
    @ApiOperation("修改学生")
    @PostMapping("/update/studentName")
    public R updateStudentName(@RequestBody Student student) {
        studentService.updateStudent(student);
        return R.ok();
    }

    /**
     * 删除学生
     * @param studentId
     * @return
     */
    @ApiOperation("删除学生")
    @PostMapping("/delete/student")
    public R deleteStudent(Long studentId) throws DeleteException {
        studentService.deleteStudent(studentId);
        return R.ok();
    }

    /**
     * 查询所有的学生，支持分页
     * @param currentPage
     * @param maxCount
     * @return
     */
    @ApiOperation("分页查询所有学生")
    @GetMapping("/query/allStudent/{currentPage}/{maxCount}")
    public R listAllStudentByPage(@PathVariable("currentPage") Integer currentPage , @PathVariable("maxCount") Integer maxCount){
        PageInfo<Student> studentPageInfo = studentService.listAllStudentByPage(currentPage, maxCount);
        return R.ok().put("data",studentPageInfo);
    }
}
