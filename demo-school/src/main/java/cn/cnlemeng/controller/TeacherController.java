package cn.cnlemeng.controller;

import cn.cnlemeng.entity.Teacher;
import cn.cnlemeng.service.TeacherService;
import cn.cnlemeng.util.PageInfo;
import cn.cnlemeng.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api(tags = "教师管理")
public class TeacherController {

    @Autowired
    private TeacherService teacherService;

    /**
     * 查询所有的老师，支持分页
     *
     * @param currentPage
     * @param maxCount
     * @return
     */
    @ApiOperation("分页查询所有教师")
    @GetMapping("/query/allTeacher/{currentPage}/{maxCount}")
    public R listAllTeacherByPage(@PathVariable("currentPage") Integer currentPage, @PathVariable("maxCount") Integer maxCount) {
        PageInfo<Teacher> teacherPageInfo = teacherService.listAllTeacherByPage(currentPage, maxCount);
        return R.ok().put("data", teacherPageInfo);
    }

    /**
     * 查询各个学科教师,支持分页
     *
     * @param currentPage
     * @param maxCount
     * @return
     */
    @ApiOperation("分页查询学科对应教师")
    @GetMapping("/query/subjectTeacher/{currentPage}/{maxCount}/{subjectId}")
    public R listSubjectTeacherByPage(@PathVariable("currentPage") Integer currentPage, @PathVariable("maxCount") Integer maxCount, @PathVariable("subjectId") Long subjectId) {
        PageInfo<Teacher> teacherPageInfo = teacherService.listSubjectTeacherByPage(currentPage, maxCount, subjectId);
        return R.ok().put("data",teacherPageInfo);
    }
}
