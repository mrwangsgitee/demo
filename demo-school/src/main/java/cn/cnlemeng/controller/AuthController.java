package cn.cnlemeng.controller;

import cn.cnlemeng.constant.SessionConstant;
import cn.cnlemeng.entity.Teacher;
import cn.cnlemeng.exception.BizCodeEnum;
import cn.cnlemeng.service.AuthService;
import cn.cnlemeng.util.R;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

@RestController
@Api(tags = "登录api")
public class AuthController {
    @Autowired
    private AuthService authService;

    /**
     * 简易登录功能
     *
     * @param teacherId
     * @param session
     * @return
     */
    @ApiOperation("用户登录")
    @PostMapping("/login")
    public R login(Long teacherId, HttpSession session) {
        Teacher teacher = authService.login(teacherId);
        if (null == teacher) {
            return R.error(BizCodeEnum.NOT_LOGIN_EXCEPTION.getCode(), BizCodeEnum.NOT_LOGIN_EXCEPTION.getMessage());
        } else {
            session.setAttribute(SessionConstant.TEACHER_ID_SESSION, teacher.getTeacherId());
            session.setAttribute(SessionConstant.TEACHER_RANK_SESSION, teacher.getTeacherRank());
            return R.ok();
        }
    }
}
