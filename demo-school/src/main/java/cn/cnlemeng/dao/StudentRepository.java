package cn.cnlemeng.dao;

import cn.cnlemeng.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author: nhsoft.wangym
 * @Date: 2022/2/18 14:09
 */
public interface StudentRepository extends JpaRepository<Student,Long> {
}
