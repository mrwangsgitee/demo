package cn.cnlemeng.dao;

import cn.cnlemeng.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @Author: nhsoft.wangym
 * @Date: 2022/2/18 14:06
 */
public interface TeacherRepository extends JpaRepository<Teacher,Long> {
}
