package cn.cnlemeng.controllerAdvice;

import cn.cnlemeng.exception.BizCodeEnum;
import cn.cnlemeng.exception.DeleteException;
import cn.cnlemeng.exception.NotLoginException;
import cn.cnlemeng.exception.PermissionException;
import cn.cnlemeng.util.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice(value = "cn.cnlemeng.controller")
public class SchoolAdvice {
    /**
     * 删除异常统一处理
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = DeleteException.class)
    public R handlerDeleteException(DeleteException e) {
        String message = e.getMessage();
        log.error(message);
        return R.error(BizCodeEnum.DELETE_EXCEPTION.getCode(), message);
    }

    /**
     * 教导主任权限异常处理
     * @param e
     * @return
     */
    @ExceptionHandler(value = PermissionException.class)
    public R handlerPermissionException(PermissionException e) {
        String message = e.getMessage();
        log.error(message);
        return R.error(BizCodeEnum.PERMISSION_EXCEPTION.getCode(), BizCodeEnum.PERMISSION_EXCEPTION.getMessage());
    }

    /**
     * 未登录异常处理
     * @param e
     * @return
     */
    @ExceptionHandler(value = NotLoginException.class)
    public R handlerNoLoginException(NotLoginException e) {
        String message = e.getMessage();
        log.error(message);
        return R.error(BizCodeEnum.NOT_LOGIN_EXCEPTION.getCode(), BizCodeEnum.NOT_LOGIN_EXCEPTION.getMessage());
    }
}
