package cn.cnlemeng.exception;

public enum BizCodeEnum {

    UNKNOWN_EXCEPTION(10000,"系统未知异常"),
    DELETE_EXCEPTION(10001,"删除操作异常"),
    NOT_LOGIN_EXCEPTION(10002,"用户未登录"),
    PERMISSION_EXCEPTION(10003,"权限不足")
    ;

    private int code;
    private String message;

    BizCodeEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
