package cn.cnlemeng.constant;

public class SessionConstant {
    public static final String TEACHER_RANK_SESSION = "teacherRank";
    public static final String TEACHER_ID_SESSION = "teacherId";
}
