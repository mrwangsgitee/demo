package cn.cnlemeng.constant;

public class TeacherConstant {
    public enum TeacherRankEnum{
        ORDINARY_TEACHER(0,"普通教师"),
        HEAD_TEACHER(1,"班主任"),
        DIRECTOR_TEACHER(2,"教导主任");

        private Integer code;
        private String message;

        TeacherRankEnum(int code, String message) {
            this.code = code;
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}
