package cn.cnlemeng.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * 用于返回
 * @param <T>
 */
@ApiModel
@Getter
@Setter
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PageInfo<T>
{
    @ApiModelProperty(value = "总条数")
    public long total_elements; // 总数
    @ApiModelProperty(value = "当前页")
    public long number; // 1 开始
    @ApiModelProperty(value = "每页条数")
    public long size;

    @ApiModelProperty(value = "数据集")
    public List<T> content;

    @JsonProperty("total_pages")
    public long totalPages() {
        long r = total_elements / size;
        if(total_elements % size != 0) {
            r += 1;
        }
        return r;
    }

    public static class Pageable
    {
        public long page_number;
        public long page_size;

    }

    @JsonProperty
    public boolean empty() {
        return content.size() == 0;
    }
    @JsonProperty
    public Pageable pageable() {
        Pageable pageable = new Pageable();
        pageable.page_number = number;
        pageable.page_size = size;
        return pageable;
    }


    public void setCurrentPage(int currentPage) {
        number = currentPage;
    }

    public void setData(List<T> data) {
        content  = data;
    }

    public void setTotalCount(int count) {
        total_elements = count;
    }

    public void setPageSize(int pageSize) {
        this.size = pageSize;
    }
    /**
     *
     * @param data  数据集
     * @param number 当前页
     * @param total  总条数
     * @param size  每页条数
     * @param <T>
     * @return
     */
    public static <T> PageInfo<T> of(List<T> data, int number, int total, int size) {
        PageInfo<T> p = new PageInfo<>();
        p.size = size;
        p.total_elements = total;
        p.number = number;
        p.content = data;
        return p;
    }
    public static <T> PageInfo<T> of(Page<T> data) {
        PageInfo<T> p = new PageInfo<>();
        p.size = data.getSize();
        p.total_elements = data.getTotalElements();
        p.number = data.getNumber();
        p.content = data.getContent();
        return p;
    }


    /**
     *  手动分页
     */
    public static <T, T1> PageInfo<T1> page(List<T> list, cn.cnlemeng.util.Page  page, Function<T, T1> func) {
        int offset = page.offset();
        List<T1> res = new ArrayList<>();
        for (int i = offset; i < offset + page.getSize() ; i++) {
            if(i >= list.size() ) {
                break;
            }
            res.add(func.apply(list.get(i)));
        }

        return of(res,
                page.getPage(), list.size(), page.getSize());
    }

    /**
     * sql查出所有条数，再手工进行分页，不在sql中进行分页！！
     * @param list
     * @param page
     * @param <T>
     * @return
     */
    public static <T> PageInfo<T> page(List<T> list, cn.cnlemeng.util.Page  page) {
        return page(list, page, Function.identity());
    }
}
