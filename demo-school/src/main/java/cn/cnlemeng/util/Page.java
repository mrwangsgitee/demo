package cn.cnlemeng.util;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *  这个page 用于接收前端请求
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Page
{


    @ApiModelProperty(example = "1")
    private int page = 1;
    @ApiModelProperty(example = "50")
    private int size = 50;

    //swagger不显示字段
    @ApiModelProperty(hidden = true)
    public int offset() {
        return (page - 1) * size;
    }

}