package cn.cnlemeng.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SubjectScoreDto {
    /**
     * 学科id
     */
    @ApiModelProperty("学科id")
    private Long subjectId;
    /**
     * 学科名字
     */
    @ApiModelProperty("学科名字")
    private String subjectName;
    /**
     * 最高分
     */
    @ApiModelProperty("最高分")
    private Double maxScore;
    /**
     * 最低分
     */
    @ApiModelProperty("最低分")
    private Double minScore;
    /**
     * 平均分
     */
    @ApiModelProperty("平均分")
    private Double avgScore;
}
