package cn.cnlemeng.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class StudentYearScoreDto {
    /**
     * 学生id
     */
    @ApiModelProperty("学生id")
    private Long studentId;
    /**
     * 学年
     */
    @ApiModelProperty("学年")
    private Integer year;
    /**
     * 成绩列表
     */
    @ApiModelProperty("成绩列表")
    List<Score> scoreLists;

    @Data
    public static class Score {
        @ApiModelProperty("分数id")
        private Long scoreId;

        @ApiModelProperty("得分")
        private Double score;

        @ApiModelProperty("学科id")
        private Long subjectId;

        @ApiModelProperty("学科名字")
        private String subjectName;
    }
}
