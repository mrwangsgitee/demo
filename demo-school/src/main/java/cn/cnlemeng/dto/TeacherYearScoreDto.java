package cn.cnlemeng.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
public class TeacherYearScoreDto {
    /**
     * 老师id
     */
    @ApiModelProperty("老师id")
    private Long teacherId;

    /**
     * 老师名字
     */
    @ApiModelProperty("老师名字")
    private String teacherName;
    /**
     * 学年
     */
    @ApiModelProperty("学年")
    private Integer year;

    /**
     * 教师学科得分列表
     */
    private List<TeacherSubjectScore> teacherSubjectScoreList;

    @Data
    public static class TeacherSubjectScore{
        /**
         * 学科id
         */
        @ApiModelProperty("学科id")

        private Long subjectId;
        /**
         * 学科名字
         */
        @ApiModelProperty("学科名字")
        private String subjectName;
        /**
         * 最高分
         */
        @ApiModelProperty("最高分")

        private Double maxScore;
        /**
         * 最低分
         */
        @ApiModelProperty("最低分")

        private Double minScore;
        /**
         * 平均分
         */
        @ApiModelProperty("平均分")

        private Double avgScore;
    }

}
