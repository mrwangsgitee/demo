package cn.cnlemeng.service;

import cn.cnlemeng.dao.TeacherRepository;
import cn.cnlemeng.entity.Teacher;
import cn.cnlemeng.util.Page;
import cn.cnlemeng.util.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@Slf4j
public class TeacherService {
    @Autowired
    private EntityManager entityManager;

    @Autowired
    private TeacherRepository teacherRepository;

    public PageInfo<Teacher> listAllTeacherByPage(Integer currentPage, Integer maxCount) {
        List<Teacher> resultList = teacherRepository.findAll();
        return PageInfo.page(resultList,new Page(currentPage,maxCount));
    }

    public PageInfo<Teacher> listSubjectTeacherByPage(Integer currentPage, Integer maxCount, Long subjectId) {
        List<Teacher> resultList = entityManager.createQuery("select a from Teacher a join TeacherSubjectRelation b on b.subjectId = :subjectId where b.teacherId = a.teacherId")
                .setParameter("subjectId", subjectId)
                .getResultList();
        return PageInfo.page(resultList,new Page(currentPage,maxCount));
    }
}
