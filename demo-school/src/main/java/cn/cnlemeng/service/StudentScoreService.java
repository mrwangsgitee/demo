package cn.cnlemeng.service;

import cn.cnlemeng.dao.TeacherRepository;
import cn.cnlemeng.entity.StudentScore;
import cn.cnlemeng.entity.Subject;
import cn.cnlemeng.entity.Teacher;
import cn.cnlemeng.exception.DeleteException;
import cn.cnlemeng.dto.StudentYearScoreDto;
import cn.cnlemeng.dto.SubjectScoreDto;
import cn.cnlemeng.dto.TeacherYearScoreDto;
import cn.cnlemeng.util.Page;
import cn.cnlemeng.util.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class StudentScoreService {
    @Autowired
    private EntityManager entityManager;

    @Autowired
    private TeacherRepository teacherRepository;

    @Transactional(rollbackFor = Exception.class)
    public void save(StudentScore studentScore) {
        entityManager.persist(studentScore);
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateById(StudentScore studentScore) {
        Long scoreId = studentScore.getScoreId();
        StudentScore res = entityManager.find(StudentScore.class, scoreId);
        res.setScore(studentScore.getScore());
        res.setStudentId(studentScore.getStudentId());
        res.setYear(studentScore.getYear());
        res.setScoreId(studentScore.getScoreId());
    }

    public StudentYearScoreDto readStudentYearScoreByIdAndYear(Long studentId, Integer year) {
        List<Object[]> resultList = entityManager.createQuery("SELECT a,b FROM StudentScore a JOIN Subject b ON a.subjectId = b.subjectId WHERE a.studentId = :id and a.year = :year")
                .setParameter("id", studentId)
                .setParameter("year", year)
                .getResultList();
        StudentYearScoreDto studentYearScoreVo = new StudentYearScoreDto();
        studentYearScoreVo.setStudentId(studentId);
        studentYearScoreVo.setYear(year);
        if (null == resultList) {
            return studentYearScoreVo;
        }
        List<StudentYearScoreDto.Score> scoreList = resultList.stream().map((o) -> {
            StudentYearScoreDto.Score score = new StudentYearScoreDto.Score();
            StudentScore studentScore = (StudentScore) o[0];
            Subject subject = (Subject) o[1];
            score.setScoreId(studentScore.getScoreId());
            score.setScore(studentScore.getScore());
            score.setSubjectId(subject.getSubjectId());
            score.setSubjectName(subject.getSubjectName());
            return score;
        }).collect(Collectors.toList());
        studentYearScoreVo.setScoreLists(scoreList);
        return studentYearScoreVo;
    }

    public TeacherYearScoreDto readTeacherYearScoreListByIdAndYear(Long teacherId, Integer year) {
        TeacherYearScoreDto teacherYearScoreDto = new TeacherYearScoreDto();
        teacherYearScoreDto.setTeacherId(teacherId);
        //查老师名字
        Teacher teacher = entityManager.find(Teacher.class, teacherId);
        teacherYearScoreDto.setTeacherName(teacher.getTeacherName());
        teacherYearScoreDto.setYear(year);
        List<Object[]> resultList = entityManager.createQuery("SELECT a.subjectId,b.subjectName FROM TeacherSubjectRelation a JOIN  Subject b ON a.subjectId = b.subjectId WHERE a.teacherId = :teacherId")
                .setParameter("teacherId", teacherId)
                .getResultList();
        if (null == resultList) {
            return teacherYearScoreDto;
        }
        List<TeacherYearScoreDto.TeacherSubjectScore> teacherSubjectScoreList = resultList.stream().map((o) -> {
            Long subjectId = (Long) o[0];
            String subjectName = (String) o[1];
            TeacherYearScoreDto.TeacherSubjectScore teacherSubjectScore = new TeacherYearScoreDto.TeacherSubjectScore();
            teacherSubjectScore.setSubjectId(subjectId);
            teacherSubjectScore.setSubjectName(subjectName);
            return teacherSubjectScore;
        }).collect(Collectors.toList());

        for (TeacherYearScoreDto.TeacherSubjectScore teacherSubjectScore : teacherSubjectScoreList) {
            Long subjectId = teacherSubjectScore.getSubjectId();
            List<Double> scoreList = entityManager.createQuery("SELECT a.score FROM StudentScore a JOIN TeacherStudentRelation b ON a.studentId = b.studentId  WHERE b.teacherId = :teacherId AND a.year = :year AND a.subjectId = :subjectId")
                    .setParameter("teacherId", teacherId)
                    .setParameter("year", year)
                    .setParameter("subjectId", subjectId)
                    .getResultList();
            Collections.sort(scoreList);
            Double maxScore = scoreList.get(scoreList.size() - 1);
            Double minScore = scoreList.get(0);
            Double avgScoreDouble = scoreList.stream().collect(Collectors.averagingDouble(Double::byteValue));
            Double avgScore = Double.valueOf(String.format("%.1f", avgScoreDouble));
            teacherSubjectScore.setAvgScore(avgScore);
            teacherSubjectScore.setMaxScore(maxScore);
            teacherSubjectScore.setMinScore(minScore);
        }
        teacherYearScoreDto.setTeacherSubjectScoreList(teacherSubjectScoreList);
        return teacherYearScoreDto;
    }

    public void deleteStudentScoreById(Long studentScoreId) {
        StudentScore studentScore = entityManager.find(StudentScore.class, studentScoreId);
        if (null == studentScore) {
            log.error("{}号学生分数删除失败，查询不存在", studentScoreId);
        }
        entityManager.remove(studentScore);
    }


    @Transactional(rollbackFor = Exception.class)
    public void batchDeleteByStudentId(Long studentId) throws DeleteException {
        //删
        entityManager.createQuery("delete from StudentScore a where a.studentId = :studentId")
                .setParameter("studentId", studentId)
                .executeUpdate();
    }


    public List<SubjectScoreDto> batchReadSubjectScoreList(Integer year) {
        List<Object[]> resultList = entityManager.createQuery("select a.subjectId,b.subjectName,max(a.score),min(a.score),round(avg(a.score),1) from StudentScore a join Subject b on a.subjectId = b.subjectId where a.year = :year group by a.subjectId")
                .setParameter("year", year)
                .getResultList();
        if (null == resultList) {
            return new ArrayList<>();
        }
        return resultList.stream().map((o) -> {
            Long subjectId = (Long) o[0];
            String subjectName = (String) o[1];
            Number maxScore = (Number) o[2];
            Number minScore = (Number) o[3];
            Number avgScore = (Number) o[4];
            SubjectScoreDto subjectScoreDto = new SubjectScoreDto();
            subjectScoreDto.setSubjectId(subjectId);
            subjectScoreDto.setSubjectName(subjectName);
            subjectScoreDto.setMaxScore(maxScore.doubleValue());
            subjectScoreDto.setMinScore(minScore.doubleValue());
            subjectScoreDto.setAvgScore(avgScore.doubleValue());
            return subjectScoreDto;
        }).collect(Collectors.toList());
    }

    public PageInfo<TeacherYearScoreDto> batchReadAllTeacherYearScoreListByYear(Integer currentPage, Integer maxCount, Integer year) {
        List<TeacherYearScoreDto> teacherYearScoreDtoList = new ArrayList<>();
        //查老师的列表
        List<Teacher> teacherList = teacherRepository.findAll();
        Map<Long, String> teacherNameMap = teacherList.stream().collect(Collectors.toMap(Teacher::getTeacherId, Teacher::getTeacherName));
        List<Long> teacherIdList = teacherList.stream().map(Teacher::getTeacherId).collect(Collectors.toList());
        //获取老师的各个学科的成绩
        List<Object[]> list = entityManager.createQuery("select b.teacherId,a.subjectId,a.subjectName,round(avg(c.score),1),max(c.score),min(c.score) from Subject a join TeacherSubjectRelation b on b.subjectId = a.subjectId join StudentScore c on c.subjectId = a.subjectId where c.year = :year and b.teacherId in :teacherIdList GROUP BY a.subjectId")
                .setParameter("year", year)
                .setParameter("teacherIdList", teacherIdList)
                .getResultList();

        for (Long id : teacherIdList) {
            TeacherYearScoreDto teacherYearScoreDto = new TeacherYearScoreDto();
            teacherYearScoreDto.setYear(year);
            teacherYearScoreDto.setTeacherId(id);
            teacherYearScoreDto.setTeacherName(teacherNameMap.get(id));
            List<TeacherYearScoreDto.TeacherSubjectScore> teacherSubjectScoreList = new ArrayList<>();
            for (Object[] o : list) {
                Long teacherId = (Long) o[0];
                if (id.equals(teacherId)) {
                    TeacherYearScoreDto.TeacherSubjectScore teacherSubjectScore = new TeacherYearScoreDto.TeacherSubjectScore();
                    Long subjectId = (Long) o[1];
                    String subjectName = (String) o[2];
                    Number avgScoreNumber = (Number) o[3];
                    Number maxScoreNumber = (Number) o[4];
                    Number minScoreNumber = (Number) o[5];
                    teacherSubjectScore.setAvgScore(avgScoreNumber.doubleValue());
                    teacherSubjectScore.setMaxScore(maxScoreNumber.doubleValue());
                    teacherSubjectScore.setMinScore(minScoreNumber.doubleValue());
                    teacherSubjectScore.setSubjectId(subjectId);
                    teacherSubjectScore.setSubjectName(subjectName);
                    teacherSubjectScoreList.add(teacherSubjectScore);
                }
            }
            teacherYearScoreDto.setTeacherSubjectScoreList(teacherSubjectScoreList);
            teacherYearScoreDtoList.add(teacherYearScoreDto);
        }
        return PageInfo.page(teacherYearScoreDtoList, new Page(currentPage, maxCount));
    }
}
