package cn.cnlemeng.service;

import cn.cnlemeng.dao.StudentRepository;
import cn.cnlemeng.entity.Student;
import cn.cnlemeng.entity.Teacher;
import cn.cnlemeng.exception.DeleteException;
import cn.cnlemeng.util.Page;
import cn.cnlemeng.util.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@Slf4j
public class StudentService {
    @Autowired
    private EntityManager entityManager;

    @Autowired
    private TeacherStudentRelationService teacherStudentRelationService;

    @Autowired
    private StudentScoreService studentScoreService;

    @Autowired
    private StudentRepository studentRepository;

    @Transactional(rollbackFor = Exception.class)
    public void saveStudent(Student student) {
        entityManager.persist(student);
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateStudent(Student student) {
        Student updateStudent = entityManager.find(Student.class, student.getStudentId());
        if (null != updateStudent){
            updateStudent.setStudentName(student.getStudentName());
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void deleteStudent(Long studentId) throws DeleteException {
        //删除学生表
        Student student = entityManager.find(Student.class, studentId);
        if (null == student) {
            log.error("删除学生失败，学生不存在");
            throw new DeleteException("删除学生失败，学生不存在");
        }
        entityManager.remove(student);
        //删除关联学生分数表
        studentScoreService.batchDeleteByStudentId(studentId);
        //删除关联学生老师关系表
        teacherStudentRelationService.batchDeleteByStudentId(studentId);
    }

    public PageInfo<Student> listAllStudentByPage(Integer currentPage, Integer maxCount) {
        List<Student> studentList = studentRepository.findAll();
        return PageInfo.page(studentList, new Page(currentPage, maxCount));
    }
}

