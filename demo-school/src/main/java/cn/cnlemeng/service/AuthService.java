package cn.cnlemeng.service;

import cn.cnlemeng.entity.Teacher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
@Slf4j
public class AuthService {
    @Autowired
    private EntityManager entityManager;

    public Teacher login(Long teacherId) {
        return entityManager.find(Teacher.class, teacherId);
    }
}
