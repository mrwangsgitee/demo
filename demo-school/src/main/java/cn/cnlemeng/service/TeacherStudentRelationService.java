package cn.cnlemeng.service;

import cn.cnlemeng.exception.DeleteException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@Slf4j
public class TeacherStudentRelationService {
    @Autowired
    private EntityManager entityManager;

    @Transactional(rollbackFor = Exception.class)
    public void batchDeleteByStudentId(Long studentId) throws DeleteException {
        //删
        entityManager.createQuery("delete from TeacherStudentRelation a where a.studentId = :studentId")
                .setParameter("studentId", studentId)
                .executeUpdate();
    }
}
