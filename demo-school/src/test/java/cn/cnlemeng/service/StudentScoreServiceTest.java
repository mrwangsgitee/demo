package cn.cnlemeng.service;

import cn.cnlemeng.entity.Student;
import cn.cnlemeng.entity.StudentScore;
import cn.cnlemeng.entity.Subject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StudentScoreServiceTest {
    @Autowired
    private EntityManager entityManager;

    @Test
    public void testReadStudentScoreListById() {
        Long studentId = 1L;
        List<Object[]> res = entityManager.createQuery("SELECT a,b FROM StudentScore a JOIN Subject b ON a.subjectId = b.subjectId WHERE a.studentId = :id")
                .setParameter("id",studentId)
                .getResultList();
        List<Object> collect = res.stream().map((o) -> {
            StudentScore studentScore = (StudentScore) o[0];
            Subject subject = (Subject) o[1];
            System.out.println(studentScore);
            System.out.println(subject);
            return null;
        }).collect(Collectors.toList());
    }
}
