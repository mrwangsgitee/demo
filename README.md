# demo

#### 介绍
入职demo

#### 软件架构
软件架构说明
基于spring boot, springmvc, jpa, mysql5.7的测试demo


#### 使用说明

1.  普通教师与教导主任查询权限基于spring拦截器实现，不同老师登录后具有不同的功能
2.  demo所有功能都已经实现


